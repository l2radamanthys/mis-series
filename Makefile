comandos:
	@echo "iniciar"

iniicar:
	@pipenv install

ejecutar:
	@pipenv run python manage.py runserver

ejecutar_worker:
	@pipenv run python manage.py rqworker

crear_migraciones:
	@pipenv run python manage.py makemigrations

migrar:
	@pipenv run python manage.py migrate --noinput
	
heroku_deploy:
	@git push heroku master

heroku_logs:
	@heroku logs --tail

reset:
	dropdb --if-exists series -e; createdb series
	pipenv run python manage.py migrate --noinput

build_requirements:
	@pipenv lock -r > requirements.txt
	cat requirements.txt
