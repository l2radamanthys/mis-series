from django.contrib import admin

from series.admin_class.series import Serie, SerieAdmin
from series.admin_class.servers import Server, ServerAdmin

# Register your models here.

admin.site.register(Serie, SerieAdmin)
admin.site.register(Server, ServerAdmin)