from django.contrib import admin
from series.models.series import Serie


class SerieAdmin(admin.ModelAdmin):
    model = Serie
    list_display = (
        'id',
        'code',
        'name',
        'status',
    )
    search_fields = ('name', 'code')