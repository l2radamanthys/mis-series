from django.contrib import admin
from series.models.servers import Server


class ServerAdmin(admin.ModelAdmin):
    model = Server
