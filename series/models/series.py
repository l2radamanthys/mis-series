from django.db import models


class Serie(models.Model):
    STATUS_CHOICES = (
        ('P', 'Proximamente'),
        ('A', 'En Emision'),
        ('F', 'Finalizada')
    )

    RANK_CHOICES = (
        (1, 'Mala'),
        (2, 'Regular'),
        (3, 'Buena'),
        (4, 'Muy Buena'),
        (5, 'Excelente'),
    )

    name = models.CharField("Nombre", max_length=200)
    code = models.CharField("Codigo", max_length=100)
    description = models.TextField("Descripción")
    rank = models.IntegerField("Calificacion", choices=RANK_CHOICES, default=2)
    image = models.ImageField("Portada", upload_to="media/uploads", default='media/uploads/default_image.png')
    chapters = models.IntegerField("Numero de Capitulos")
    status = models.CharField("Estado", max_length=1, choices=STATUS_CHOICES)
    gender = models.CharField("Genero", max_length=150)

    class Meta:
        db_table = "Series"
        verbose_name = "Serie"
        verbose_name_plural = "Series"
        ordering = ['code', 'name', '-id']

    def __str__(self):
        return self.name